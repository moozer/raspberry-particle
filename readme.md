Usage
-----

Update the files `scripts/update_image.sh` and `scripts/copy_files.sh` to reflect the changes you want to the default raspberry pi image.


Testing
---

Download to local cache

```bash
gitlab-runner exec docker --docker-pull-policy="if-not-present" --docker-privileged="true" --cache-dir=/tmp/gitlab-cache --docker-cache-dir=/tmp/gitlab-cache --docker-volumes=/tmp/gitlab-cache "download"
```

build

```bash
gitlab-runner exec docker --docker-pull-policy="if-not-present" --docker-privileged="true" --cache-dir=/tmp/gitlab-cache --docker-cache-dir=/tmp/gitlab-cache --docker-volumes=/tmp/gitlab-cache "update image bin"
```


Design/issues
--------

### Ansible and proot

Ansible and proot has some issues related to success of certain tasks like `apt install ...` and about the "writability" of files.

The latter, I think is related to proot mapping to uid 0, so creating files will be shown as being by diferent users than specified in e.g. a creation task.

### fuseext2 and proot

`Fuseext2` in rw mode is brittle, and it fails very often when being used in conjunction with proot.

## current solution
`guestmount` is my prefered alternative, but that doesn't work in docker. The current solution includes `mount -o loop ...`.

The CI part is two-fold, the first is "download", that checks if there is a new verison available compared to the cache. The other part is the build part.

This is not relevant when running purely on gitlab, since caches in general get missed, and everything needs to be downloaded again. It is relevant wehn testing locally or on your own gitlab runners.

I have also added a deploy step to put the image on gitlab pages.
