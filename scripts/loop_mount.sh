#!/usr/bin/env bash

set -e

if [ "x" == "x$1" ]; then
    echo "usage $0 <full image>"
    exit 1
fi

MNTDIR="mnt"
FULL_IMG=$1

# see https://gitlab.com/gitlab-com/support-forum/issues/3732
# echo loopback noes on device
# ls -al /dev/loop*
#
# echo "new dev dir"
#  mkdir -p /tmp/dev
#  mount -t devtmpfs none /tmp/dev
#


# https://blog.tinned-software.net/mount-raw-image-of-entire-disc/
echo "image filetype"
file $FULL_IMG

# extracting geometry from image file
sector_size=$(/sbin/fdisk -l $FULL_IMG | grep "Sector size" | cut -d' ' -f 4)
offset_root=$(/sbin/fdisk -l $FULL_IMG | grep "img2" | awk -F'\ *' '{print $2}')
sectors_root=$(/sbin/fdisk -l $FULL_IMG | grep "img2" | awk -F'\ *' '{print $4}')

offset_boot=$(/sbin/fdisk -l $FULL_IMG | grep "img1" | awk -F'\ *' '{print $2}')
sectors_boot=$(/sbin/fdisk -l $FULL_IMG | grep "img1" | awk -F'\ *' '{print $4}')
echo "- Root partition at $offset_root*$sector_size and boot partition at $offset_boot*$sector_size"

echo "- loop device usage"
losetup -l

echo "- create nodes in /dev if not already exists"
if [ ! -e /dev/loop0 ]; then
 for i in $(seq 0 9); do
    mknod -m 0660 "/dev/loop$i" b 7 "$i"
 done
fi

echo "- loop devices in /dev"
ls -al /dev/loop*

LOOPDEV_ROOT=$(losetup -f)
echo "- root partition on $LOOPDEV_ROOT"
losetup $LOOPDEV_ROOT --offset $(($offset_root*$sector_size)) --sizelimit $(($sectors_root*$sector_size)) $FULL_IMG
mount $LOOPDEV_ROOT $MNTDIR

LOOPDEV_BOOT=$(losetup -f)
echo "- boot partition on $LOOPDEV_BOOT"
losetup $LOOPDEV_BOOT --offset $(($offset_boot*$sector_size)) --sizelimit $(($sectors_boot*$sector_size)) $FULL_IMG
mount $LOOPDEV_BOOT $MNTDIR/boot

# the easy way doesn't wok on gitlab.com
# "mount:
#        /builds/moozer/raspberry-image-static/raspberry-latest-img/raspbian_lite_latest.img:
#        failed to setup loop device: No such file or directory"
# https://gitlab.com/moozer/raspberry-image-static/-/jobs/219466237
#echo mounting root
#mount -t ext4 -o loop,offset=$(($offset_root*$sector_size)),sizelimit=$(($sectors_root*$sector_size)) $FULL_IMG $MNTDIR
#echo mounting boot
#mount -o loop,offset=$(($offset_boot*$sector_size)),sizelimit=$(($sectors_boot*$sector_size)) $FULL_IMG $MNTDIR/boot



# echo mount ${LOOPDEV}p1 $MNTDIR/boot/
# mount ${LOOPDEV}p1 $MNTDIR/boot/


#
# echo "adding partitions to loop"
# for i in seq 10; do
#
# LOOPDEV=$(losetup -f)
#
# echo "loop devicd: $LOOPDEV"
# ls -al /dev/loop*
#
#
#
# losetup $LOOPDEV -P $FULL_IMG
#
#
# echo "list of loop devices in use"
# losetup -l
# #  LOOPDEV=$(losetup -l | grep $PART_IMG | cut -f1 -d' ' | grep -v deleted)
#
# echo mounting
# echo mount -t ext4 ${LOOPDEV}p2 $MNTDIR
# mount -t ext4 ${LOOPDEV}p2 $MNTDIR
# echo mount ${LOOPDEV}p1 $MNTDIR/boot/
# mount ${LOOPDEV}p1 $MNTDIR/boot/
