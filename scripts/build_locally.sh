#!/usr/bin/env bash

DL_NAME="raspbian_lite_latest"
DL_CACHE_DIR="raspberry-latest-img"
IMAGE_ZIP="raspbian_lite_latest.zip"
IMAGE_FILE="raspbian_lite_static.img"
BUILD_BASE_NAME="raspbian_lite_static"

mkdir -p $DL_CACHE_DIR

if [ ! -f $DL_CACHE_DIR/$IMAGE_FILE ]; then
  echo "** Downloading"
  bash ./scripts/download_and_unzip.sh $DL_CACHE_DIR $IMAGE_FILE
else
  echo "** not downloading - using cache";
fi

echo "** file info"
file $DL_CACHE_DIR/$IMAGE_FILE

echo "** mount and update"
bash ./scripts/mount_and_update.sh $DL_CACHE_DIR/$IMAGE_FILE

echo "** unmount and zip"
bash ./scripts/unmount_and_zip.sh $DL_CACHE_DIR/$IMAGE_FILE $DL_CACHE_DIR/${BUILD_BASE_NAME}.zip

echo "** calculate sha256"
sha256sum $DL_CACHE_DIR/${DL_NAME}.zip > $DL_CACHE_DIR/${BUILD_BASE_NAME}.sha256

echo "** result"
ls -al $DL_CACHE_DIR
